<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        $adminRole = Role::create(['name' => 'admin']);
        $userRole = Role::create(['name' => 'user']);

        $permissions = [
            ['name' => 'view_users', 'guard_name' => 'api', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'add_users', 'guard_name' => 'api', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'update_users', 'guard_name' => 'api', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'delete_users', 'guard_name' => 'api', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'view_schedule', 'guard_name' => 'api', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'add_schedule', 'guard_name' => 'api', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'update_schedule', 'guard_name' => 'api', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'delete_schedule', 'guard_name' => 'api', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'view_weekly_plans', 'guard_name' => 'api', 'created_at' => $now, 'updated_at' => $now],
        ];
        Permission::query()->insert($permissions);

        $adminRole->givePermissionTo(Permission::all());
        $userRole->givePermissionTo([
            ['name' => 'view_schedule'],
            ['name' => 'add_schedule'],
            ['name' => 'update_schedule'],
            ['name' => 'delete_schedule'],
            ['name' => 'view_weekly_plans'],
        ]);
    }
}
