<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()
            ->create([
                'username' => 'admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('password'),
            ])->assignRole('admin');

        User::query()
            ->create([
                'username' => 'user',
                'email' => 'user@user.com',
                'password' => bcrypt('password'),
            ])->assignRole('user');
    }
}
