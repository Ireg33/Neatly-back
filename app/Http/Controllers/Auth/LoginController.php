<?php

namespace App\Http\Controllers\Auth;

use GuzzleHttp\Client;
use App\Traits\ApiTrait;
use App\Helpers\ApiCodes;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Client as OClient;
use Illuminate\Support\Facades\Request as DomainRequest;

class LoginController extends Controller
{
    use ApiTrait;

    /**
     * @param LoginRequest $request
     *
     * @return array|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function login(LoginRequest $request)
    {
        $data = $request->validated();
        $usernameField = $data['username'];

        $usernameColumn = filter_var($usernameField, FILTER_VALIDATE_EMAIL)
            ? 'email'
            : 'username';

        $login = [
            $usernameColumn => $usernameField,
            'password' => $data['password'],
        ];

        if (! Auth::attempt($login)) {
            return $this->responseToJson(
                'Përdoruesi ose fjalëkalimi është i gabuar! Ju lutem provoni përsëri',
                ApiCodes::BAD_REQUEST
            );
        }

        $user = Auth::user();

        $oClient = OClient::where('password_client', 1)->first();
        $tokens = $this->getTokenAndRefreshToken($oClient, $user->email, $request->password);

        $message = ApiCodes::getSuccessMessage();
        $statusCode = ApiCodes::SUCCESS;

        return $this->responseToJson($message, $statusCode, [
            'user' => UserResource::make($user),
            'access_token' => $tokens->original['access_token'],
            'refresh_token' => $tokens->original['refresh_token'],
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse|mixed
     */
    public function refreshToken(Request $request)
    {
        $refreshToken = $request->header('Refreshtoken');
        $oClient = OClient::where('password_client', 1)->first();
        $http = new Client();

        $domain = DomainRequest::getHttpHost();

        try {
            $response = $http->request('POST', $domain . '/oauth/token', [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $refreshToken,
                    'client_id' => $oClient->id,
                    'client_secret' => $oClient->secret,
                    'scope' => '*',
                ],
            ]);

            $result = json_decode((string) $response->getBody(), true);
            $message = ApiCodes::getSuccessMessage();
            $statusCode = ApiCodes::SUCCESS;

            return $this->responseToJson($message, $statusCode, $result);
        } catch (\Exception $e) {
            return $this->responseToJson(
                'Unauthorized',
                ApiCodes::UNAUTHENTICATED
            );
        }
    }

    /**
     * @param OClient $oClient
     * @param $email
     * @param $password
     *
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTokenAndRefreshToken(OClient $oClient, $email, $password)
    {
        $http = new Client();
        $domain = DomainRequest::getHttpHost();

        $response = $http->request('POST', $domain . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => $oClient->id,
                'client_secret' => $oClient->secret,
                'username' => $email,
                'password' => $password,
                'scope' => '*',
            ],
        ]);

        $result = json_decode((string) $response->getBody(), true);
        return response()->json($result);
    }

    /**
     * @return JsonResponse
     */
    public function logout()
    {
//        auth()->user()->revokeToken();
        //TODO

        return $this->responseToJson(ApiCodes::getSuccessMessage(), ApiCodes::SUCCESS, []);
    }
}
