<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ApiCodes;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateScheduleRequest;
use App\Http\Requests\UpdateScheduleRequest;
use App\Http\Resources\UserScheduleListResource;
use App\Models\UserSchedule;
use App\Traits\ApiTrait;
use Facade\FlareClient\Http\Exceptions\NotFound;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;


class ScheduleController extends Controller
{
    use ApiTrait;

    public function index(): JsonResponse
    {
        if (! auth()->user()->hasPermissionTo('view_schedule')) {
            return $this->generalError();
        }

        $currentUserId = Auth::user()->id;

        $userSchedule = UserSchedule::all()->where('user_id', '=', $currentUserId);

        if ($userSchedule->isEmpty()) {
            return $this->resourceNotFound(null, ApiCodes::SUCCESS);
        }

        return $this->getJsonResponse($userSchedule, UserScheduleListResource::class);
    }

    public function show($scheduleId): JsonResponse
    {
        if (! auth()->user()->hasPermissionTo('view_schedule')) {
            return $this->generalError();
        }

        try {
            $schedule = UserSchedule::find($scheduleId);

            if ($schedule === null || $schedule->user_id != Auth::user()->id){
                return $this->resourceNotFound(null, ApiCodes::SUCCESS);
            }

            return $this->getJsonResponse($schedule, UserScheduleListResource::class);
        } catch (NotFound $e) {
            return $this->resourceNotFound($e->getMessage());
        }
    }

    public function store(CreateScheduleRequest $request): JsonResponse
    {
        if (! auth()->user()->hasPermissionTo('add_schedule')) {
            return $this->generalError();
        }

        $validated = $request->validated();

        try {
            UserSchedule::query()->create($validated);

            return $this->successResponse();
        } catch (\Exception $e) {
            return $this->generalError($e->getMessage());
        }
    }

    public function update(UpdateScheduleRequest $request, $scheduleId): JsonResponse
    {
        if (! auth()->user()->hasPermissionTo('update_schedule')) {
            return $this->generalError();
        }

        $validated = $request->validated();

        try {
            $schedule = UserSchedule::find($scheduleId);

            if ($schedule->user_id != Auth::user()->id){
                return $this->resourceNotFound(null, ApiCodes::SUCCESS);
            }

            $schedule->update($validated);

            return $this->successResponse();
        } catch (NotFound $e) {
            return $this->resourceNotFound($e->getMessage());
        } catch (\Exception $e) {
            return $this->generalError();
        }
    }

    public function destroy($scheduleId): JsonResponse
    {
        if (! auth()->user()->hasPermissionTo('delete_schedule')) {
            return $this->generalError();
        }

        try {
            $schedule = UserSchedule::find($scheduleId);

            if ($schedule === null || $schedule->user_id != Auth::user()->id){
                return $this->resourceNotFound(null, ApiCodes::SUCCESS);
            }
            $schedule->delete();

            return $this->successResponse();
        } catch (NotFound $e) {
            return $this->resourceNotFound($e->getMessage());
        }
    }
}
