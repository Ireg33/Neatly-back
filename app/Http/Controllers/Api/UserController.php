<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ApiCodes;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserListResource;
use App\Models\User;
use App\Traits\ApiTrait;
use Facade\FlareClient\Http\Exceptions\NotFound;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    use ApiTrait;

    public function index(): JsonResponse
    {
        $users = User::all();

        if ($users->isEmpty()) {
            return $this->resourceNotFound(null, ApiCodes::SUCCESS);
        }

        return $this->getJsonResponse($users, UserListResource::class);
    }

    public function show($userId): JsonResponse
    {
        try {
            $user = User::find($userId);

            return $this->getJsonResponse($user, UserListResource::class);
        } catch (NotFound $e) {
            return $this->resourceNotFound($e->getMessage());
        }
    }

    public function store(CreateUserRequest $request): JsonResponse
    {
        if (! auth()->user()->hasPermissionTo('add_users')) {
            return $this->generalError();
        }

        $validated = $request->validated();
        $validated['password'] = bcrypt($validated['password']);

        try {
            User::query()->create($validated)->assignRole('user');

            return $this->successResponse();
        } catch (\Exception $e) {
            return $this->generalError($e->getMessage());
        }
    }

    public function update(UpdateUserRequest $request, $userId): JsonResponse
    {
        if (! auth()->user()->hasPermissionTo('update_users')) {
            return $this->generalError();
        }

        $validated = $request->validated();

        try {
            $user = User::find($userId);
            $user->update($validated);

            return $this->successResponse();
        } catch (NotFound $e) {
            return $this->resourceNotFound($e->getMessage());
        } catch (\Exception $e) {
            return $this->generalError();
        }
    }

}
