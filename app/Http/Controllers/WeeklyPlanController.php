<?php

namespace App\Http\Controllers;

use App\Helpers\ApiCodes;
use App\Http\Resources\UserScheduleListResource;
use App\Models\UserSchedule;
use App\Traits\ApiTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WeeklyPlanController extends Controller
{
    use ApiTrait;

    public function index()
    {
        if (! auth()->user()->hasPermissionTo('view_schedule')) {
            return $this->generalError();
        }

        $currentUserId = Auth::user()->id;

        $now = Carbon::now();
        $weekStartDate = $now->startOfWeek()->format('Y-m-d');
        $weekEndDate = $now->endOfWeek()->format('Y-m-d');

        $userSchedule = UserSchedule::all()
            ->where('user_id', '=', $currentUserId)
            ->where('to_date', '<=', $weekEndDate)
            ->where('from_date', '>=', $weekStartDate);

        if ($userSchedule->isEmpty()) {
            return $this->resourceNotFound(null, ApiCodes::SUCCESS);
        }

        return $this->getJsonResponse($userSchedule, UserScheduleListResource::class);
    }
}
