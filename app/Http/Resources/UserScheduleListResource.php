<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserScheduleListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user_id" => $this->user_id,
            "priority" => $this->priority,
            "start" => $this->from_date,
            "end" => $this->to_date,
            "name" => $this->title,
            "details" => $this->details,
            "color" => $this->color,
        ];
    }
}
