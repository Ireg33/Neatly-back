<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSchedule extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'user_schedule';
    protected $guarded = [];

    const HIGH_PRIORITY = 'HIGH';
    const MEDIUM_PRIORITY = 'MEDIUM';
    const LOW_PRIORITY = 'LOW';
}
