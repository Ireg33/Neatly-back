<?php

namespace App\Helpers;

class ApiCodes
{
    public const SUCCESS = 200;
    public const ACCEPTED = 202;
    public const BAD_REQUEST = 400;
    public const RESOURCE_NOT_FOUND = 404;
    public const FORBIDDEN = 403;
    public const RESOURCE_CAN_NOT_BE_MODIFIED = 5;
    public const UNAUTHENTICATED = 401;
    public const METHOD_NOT_ALLOWED = 405;
    public const INTERNAL_SERVER_ERROR = 500;
    public const SERVICE_UNAVAILABLE = 503;

    public static function getSuccessMessage(): string
    {
        return 'Veprimi u realizua me sukses!';
    }

    public static function getResourceNotFoundMessage(): string
    {
        return 'Nuk u gjet asnjë e dhënë!';
    }

    public static function getGeneralErrorMessage(): string
    {
        return 'Gabim i brëndshem! Ju lutem provoni përsëri!';
    }

    public static function getModelSoftDeletedMessage(): string
    {
        return 'Ekziston një entitet i fshirë me këtë emër!';
    }

    public static function getForbiddenErrorMessage(): string
    {
        return 'Ju nuk lejoheni ta kryeni këtë veprim!';
    }

    public static function getWrongCredentialsMessage(): string
    {
        return 'Kredencialet e vendosura nuk janë te sakta!';
    }

    public static function getRecordExistsErrorMessage(): string
    {
        return 'Ky entitet ekziston në databazë!';
    }
}
