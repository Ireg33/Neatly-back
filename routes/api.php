<?php

use App\Http\Controllers\Api\ScheduleController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\WeeklyPlanController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', [LoginController::class , 'login']);
Route::post('/refreshtoken', 'LoginController.php@refreshToken');

Route::group(['middleware' => 'auth:api'], function () {
    Route::resource('/users', UserController::class);
    Route::resource('/schedule', ScheduleController::class);
    Route::get('/weekly-plan', [WeeklyPlanController::class, 'index']);
});
